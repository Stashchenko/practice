from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import get_object_or_404
from django.views.generic.edit import FormView
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from .models import Question, Choice
from django.shortcuts import render
from django.core.files import File
from django.forms import ModelForm
from django.views import generic
from django.urls import reverse
import datetime

def index(request):
	model = Question
	# Генерация "количеств" некоторых главных объектов
	num_questions=Question.objects.all().count()
	num_votes = 0
	
	for i in Choice.objects.all():
		num_votes+=i.votes
	#num_votes=Question.objects.all().count()
	# Доступные книги (статус = 'a')
	#num_instances_available=BookInstance.objects.filter(status__exact='a').count()
	#num_authors=Author.objects.count()  # Метод 'all()' применен по умолчанию.
	return render(
		request,
		'index.html',
		context={
			'num_questions':num_questions,
			'num_votes':num_votes,
			},)

class RegisterFormView(FormView):
	form_class = UserCreationForm
	success_url = '/'
	
	template_name = "golosovalka/register.html"
	
	def form_valid(self, form):
		form.save()
		return super(RegisterFormView, self).form_valid(form)
	
	def form_invalid(self, form):
		form.save()
		return super(RegisterFormView, self).form_invalid(form)

# ~ class PollForm(ModelForm):
    # ~ class Meta:
        # ~ model = Question

# ~ class QuestionCreate(CreateView):
	# ~ def post(self, request, *args, **kw):
		# ~ form = PollForm(request.POST)
		# ~ if form.is_valid():
			# ~ app = form.save()
			# ~ return HttpResponseRedirect(reverse('polls:question', args=(app.id,)))
		# ~ else:
			# ~ return super(CreateAppView, self).post(request, *args, **kw)

class QuestionCreate(CreateView):
	model = Question
	fields = ['question_text','pub_date','upload']
	initial = {'pub_date': datetime.datetime.now(),}

class QuestionUpdate(UpdateView):
	model = Question
	fields = []

class QuestionDelete(DeleteView):
	model = Question
	success_url = reverse_lazy('polls:questions')

class ChoiceCreate(CreateView):
	model = Choice
	fields = ['question','choice_text']
	initial = {'pub_date': datetime.datetime.now(),}

class ChoiceUpdate(UpdateView):
	model = Choice
	fields = ['question','choice_text']

class ChoiceDelete(DeleteView):
	model = Choice
	success_url = reverse_lazy('polls:questions')

class QuestionListView(generic.ListView):
	model = Question
	paginate_by = 10
	def get_context_data(self, **kwargs):
		# В первую очередь получаем базовую реализацию контекста
		context = super(QuestionListView, self).get_context_data(**kwargs)
		return context

class MyQuestionListView(LoginRequiredMixin,generic.ListView):
	model = Question
	template_name ='golosovalka/myquestions.html'
	paginate_by = 10
	
	def get_queryset(self):
		return Question.objects.all().order_by('pub_date')

class ResultsView(generic.DetailView):
	model = Question
	template_name = 'golosovalka/results.html'

class StatisticView(generic.DetailView):
	model = Question
	template_name = 'golosovalka/results.html'

def check_vote(request,pk):
	question = get_object_or_404(Question, pk=pk)
	usr = request.user
	if (usr in question.users.all()):
		return HttpResponseRedirect(reverse('polls:results', args=(pk,)))
	else:
		return render(request, 'golosovalka/detail.html', {'question': question,})

def stat_view(request):
	q = Question.objects.all()
	vmin = 0
	vmax = 0
	tmin = ''
	tmax = ''
	qu = q[0]
	for i in q:
		clist = i.choice_set.all()
		vlist = {}
		for j in clist:
			vlist.update({j.choice_text:j.votes})
		vlist = sorted(vlist.items(), key=lambda x: x[1], reverse=True)
		if (vlist[0][1]>vmax):
			qu = i
			vmax = vlist[0][1]
			tmax = vlist[0][0]
			vmin = vlist[-1][1]
			tmin = vlist[-1][0]
	return render(request, 'golosovalka/stat.html', {'question': qu,'vmin':vmin,'vmax':vmax,'tmax':tmax,'tmin':tmin})


def vote(request, question_id):
	question = get_object_or_404(Question, pk=question_id)
	usr = request.user
	if (usr in question.users.all()):
		# ~ print(request.user)
		# ~ print(question.users.all())
		# ~ print("IN")
		#return render(request,'golosovalka/results.html','question',question) #render(request, 'golosovalka/vote.html', {'question': question,'error_message': "You are already vote",})
		return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
	try:
		selected_choice = question.choice_set.get(pk=request.POST['choice'])
		selected_choice.votes += 1
		selected_choice.save()
		question.users.add(usr)
		return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
	except (KeyError, Choice.DoesNotExist):
		# Redisplay the question voting form.
		return render(request, 'golosovalka/vote.html', {
			'question': question,
			'error_message': "You didn't select a choice.",
		})
