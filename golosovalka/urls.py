from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'polls'
urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^register/$', views.RegisterFormView.as_view(), name='register'),
	url(r'^questions/$', views.QuestionListView.as_view(), name='questions'),
	url(r'^myquestions/$', views.MyQuestionListView.as_view(), name='myquestions'),
	url(r'^question/(?P<pk>\d+)/$', views.check_vote, name='detail'),
	url(r'^question/(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
	url(r'^question/(?P<pk>\d+)/results/$', views.ResultsView.as_view(), name='results'),
	url(r'^statistic/$', views.stat_view, name='statistic'),
    url(r'^question/create/$', views.QuestionCreate.as_view(), name='question_create'),
    url(r'^question/(?P<pk>\d+)/update/$', views.QuestionUpdate.as_view(), name='question_update'),
    url(r'^question/(?P<pk>\d+)/delete/$', views.QuestionDelete.as_view(), name='question_delete'),
    url(r'^choice/create/$', views.ChoiceCreate.as_view(), name='choice_create'),
    url(r'^choice/(?P<pk>\d+)/update/$', views.ChoiceUpdate.as_view(), name='choice_update'),
    url(r'^choice/(?P<pk>\d+)/delete/$', views.ChoiceDelete.as_view(), name='choice_delete'),
]
