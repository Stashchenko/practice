from django.contrib.auth.models import User
from django.urls import reverse
from django.db import models
import datetime
import django.utils.timezone as tnow
def user_directory_path():
	return 'polls/'


class Question(models.Model):
	question_text = models.CharField(max_length=200)
	pub_date = models.DateTimeField('date published',default=tnow.now()) #datetime.datetime.now()
	upload = models.ImageField(upload_to='polls/',default='polls/def_img.png')
	users = models.ManyToManyField(User, help_text="Users who vote",blank=True)
	def was_published_recently(self):
		#now = datetime.datetime.now()
		#return now - datetime.timedelta(days=1) <= self.pub_date <= now
		return True
	was_published_recently.admin_order_field = 'pub_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'
	def __str__(self):
		return self.question_text
	def get_absolute_url(self):
		return reverse('polls:detail', args=[str(self.id)])
	class Meta:
		ordering = ["-pub_date"]
		permissions = (("can_create_qc", "Create question and choices"),)  

class Choice(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=200)
	votes = models.IntegerField(default=0)
	def __str__(self):
		return self.choice_text
	def vcount(self):
		return self.votes
